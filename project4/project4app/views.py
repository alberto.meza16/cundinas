from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

from django import forms
from .forms import Cform, Cform2 , Cform3

from .models import Cundinas,Persona,Managers

# Create your views here.
def index (request):
    return render(request,"home/CRUD_list.html",{})

def create(request):
    form = Cform(request.POST or None)
    message="Ingresa"
    if request.user.is_authenticated:
        message ="Acceso"
        if form.is_valid():
           instance = form.save(commit=False)
           form.save()
           return redirect('/')

        else:
           error = "log in"
    context ={"form":form,"message":error}
    return render(request,"home/create.html",context)

def create2(request):
    form = Cform2(request.POST or None)
    message="Ingresa"
    if request.user.is_authenticated:
        message ="Acceso"
        if form.is_valid():
           instance = form.save(commit=False)
           form.save()
           return redirect('/')

        else:
           error = "log in"
    context ={"form":form,"message":error}
    return render(request,"home/create.html",context)

def register(response):
    if response.method == "POST":
        form = UserCreationForm(response.POST)
        if form.is_valid():
            form.save()

        return redirect("/")
    else:
        form = UserCreationForm()
    return render(response,"home/register.html",{"form":form})

def lista(request):
    Cundina =Cundinas.objects.all()
    return render(request,"home/here.html",{'Cundina':Cundina})



def list(request):
    context= {
    "Cundinas_list":Cundinas.objects.all()
    }
    return render(request,"home/CRUD_lista.html",context)

def list2(request):
    context= {
    "Personas_list":Persona.objects.all()
    }
    return render(request,"home/detail.html",context)

def list3(request):
    context= {
    "Managers_list":Managers.objects.all()
    }
    return render(request,"home/detail2.html",context)




def showthis(request):
    users = User.objects.all()
    context ={"users":users}

    return render(request, 'home/CRUD_list.html', context)

def detail(request ,id):
    queryset= Cundinas.objects.get(numero=id)
    context = {"object":queryset}
    return render (request,"home/cundinas_list.html",context)

def lista2(request):
    Personaz =Persona.objects.all()
    return render(request,"home/here2.html",{'Personaz':Personaz})

def detail22(request ,id):
    persona= Persona.objects.get(id=id)
    form =Cform2(request.POST or None, instance=persona)
    if form.is_valid():
        form.save()
        return redirect('lista2')
    return render(request, "home/here3.html",{"form":form,"persona":persona})

def detail2(request ,id):
    queryset= Persona.objects.get(numero=id)
    context = {"object":queryset}
    return render (request,"home/personas_list.html",context)


def detail3(request ,id):
    queryset= Managers.objects.get(manager=id)
    context = {"object":queryset}
    return render (request,"home/managers_list.html",context)

def has_add_permission(self, request):
    return False

def has_delete_permission(self, request, obj=None):
    return False
