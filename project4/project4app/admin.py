from django.contrib import admin

# Register your models here.
from .models import Cundinas
admin.site.register(Cundinas)

from .models import Persona
admin.site.register(Persona)
